```
Started by GitLab push by Pangalinan Jimmy
Obtained Jenkinsfile-hello-world.groovy from git https://10.1.89.234/jpangalinan/jenkins-file.git
[Pipeline] Start of Pipeline
[Pipeline] node
Running on Jenkins in /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Declarative: Checkout SCM)
[Pipeline] checkout
Selected Git installation does not exist. Using Default
The recommended git tool is: NONE
using credential GITLAB_POC
 > git rev-parse --resolve-git-dir /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/.git # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url https://10.1.89.234/jpangalinan/jenkins-file.git # timeout=10
Fetching upstream changes from https://10.1.89.234/jpangalinan/jenkins-file.git
 > git --version # timeout=10
 > git --version # 'git version 1.8.3.1'
using GIT_ASKPASS to set credentials GITLAB_POC
Setting http proxy: 10.1.92.74:9090
 > git fetch --tags --progress https://10.1.89.234/jpangalinan/jenkins-file.git +refs/heads/*:refs/remotes/origin/* # timeout=10
skipping resolution of commit refs/tags/1.0.1, since it originates from another repository
 > git rev-parse refs/remotes/origin/main^{commit} # timeout=10
Checking out Revision 718de3412ba1ba1054e31e09ab97a913f5704657 (refs/remotes/origin/main)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 718de3412ba1ba1054e31e09ab97a913f5704657 # timeout=10
Commit message: "Update file Jenkinsfile-hello-world.groovy"
 > git rev-list --no-walk 718de3412ba1ba1054e31e09ab97a913f5704657 # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] withEnv
[Pipeline] {
[Pipeline] stage
[Pipeline] { (pull repo)
[Pipeline] dir
Running in /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99
[Pipeline] {
[Pipeline] checkout
Selected Git installation does not exist. Using Default
The recommended git tool is: NONE
using credential GITLAB_POC
Cloning the remote Git repository
Cloning repository https://10.1.89.234/jpangalinan/hello-world.git
 > git init /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99 # timeout=10
Fetching upstream changes from https://10.1.89.234/jpangalinan/hello-world.git
 > git --version # timeout=10
 > git --version # 'git version 1.8.3.1'
using GIT_ASKPASS to set credentials GITLAB_POC
Setting http proxy: 10.1.92.74:9090
 > git fetch --tags --progress https://10.1.89.234/jpangalinan/hello-world.git +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git config remote.origin.url https://10.1.89.234/jpangalinan/hello-world.git # timeout=10
 > git config --add remote.origin.fetch +refs/heads/*:refs/remotes/origin/* # timeout=10
Avoid second fetch
skipping resolution of commit refs/tags/1.0.1, since it originates from another repository
 > git rev-parse refs/remotes/origin/refs/tags/1.0.1^{commit} # timeout=10
 > git rev-parse refs/tags/1.0.1^{commit} # timeout=10
Checking out Revision 9021920223999a462f720fa5957cbb665597a76f (refs/tags/1.0.1)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 9021920223999a462f720fa5957cbb665597a76f # timeout=10
Commit message: "Update file package.json"
 > git rev-list --no-walk 06cbc9b7684f5b5d9261e043b649e4fcb632dab2 # timeout=10
[Pipeline] script
[Pipeline] {
[Pipeline] readJSON
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // dir
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Build Artifact)
[Pipeline] nodejs
[Pipeline] {
[Pipeline] dir
Running in /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99
[Pipeline] {
[Pipeline] echo
Build Artifact
[Pipeline] sh
+ npm install 

added 290 packages, and audited 291 packages in 13s

31 packages are looking for funding
  run `npm fund` for details

133 low severity vulnerabilities

To address issues that do not require attention, run:
  npm audit fix

To address all issues (including breaking changes), run:
  npm audit fix --force

Run `npm audit` for details.
[Pipeline] sh
+ npm run test

> hello-world@1.0.10 test
> jest --coverage

PASS ./app.test.js
  FizzBuzz
    ✓ [3] should result in "fizz" (5 ms)
    ✓ [5] should result in "buzz" (1 ms)
    ✓ [15] should result in "fizzbuzz" (5 ms)
    ✓ [1,2,3] should result in "1, 2, fizz" (6 ms)

----------|---------|----------|---------|---------|-------------------
File      | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s 
----------|---------|----------|---------|---------|-------------------
All files |    90.9 |    83.33 |     100 |    90.9 |                   
 app.js   |    90.9 |    83.33 |     100 |    90.9 | 12                
----------|---------|----------|---------|---------|-------------------
Test Suites: 1 passed, 1 total
Tests:       4 passed, 4 total
Snapshots:   0 total
Time:        0.648 s
Ran all test suites.
[Pipeline] }
[Pipeline] // dir
[Pipeline] }
[Pipeline] // nodejs
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Code Quality Check via SonarQube)
[Pipeline] script
[Pipeline] {
[Pipeline] nodejs
[Pipeline] {
[Pipeline] tool
[Pipeline] withSonarQubeEnv
Injecting SonarQube environment variables using the configuration: sonarqube-dev
[Pipeline] {
[Pipeline] dir
Running in /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99
[Pipeline] {
[Pipeline] tool
[Pipeline] sh
+ /var/lib/jenkins/sonar-scanner/bin/sonar-scanner -Dsonar.projectKey=hello-world -Dsonar.sources=src -Dsonar.javascript.lcov.reportPaths=./coverage/lcov.info -Dsonar.host.url=http://10.1.89.220:9000 -Dsonar.login=sqa_7186f0531c72bb25612959e302c7e7ad05b54d33
INFO: Scanner configuration file: /var/lib/jenkins/sonar-scanner/conf/sonar-scanner.properties
INFO: Project root configuration file: /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99/sonar-project.properties
INFO: SonarScanner 5.0.1.3006
INFO: Java 17.0.7 Eclipse Adoptium (64-bit)
INFO: Linux 3.10.0-1160.71.1.el7.x86_64 amd64
INFO: User cache: /var/lib/jenkins/.sonar/cache
INFO: Analyzing on SonarQube server 9.9.2.77730
INFO: Default locale: "en_US", source code encoding: "UTF-8"
INFO: Load global settings
INFO: Load global settings (done) | time=87ms
INFO: Server id: 147B411E-AYsIXleTa9CQEzINxJeU
INFO: User cache: /var/lib/jenkins/.sonar/cache
INFO: Load/download plugins
INFO: Load plugins index
INFO: Load plugins index (done) | time=39ms
INFO: Load/download plugins (done) | time=138ms
INFO: Process project properties
INFO: Process project properties (done) | time=7ms
INFO: Execute project builders
INFO: Execute project builders (done) | time=1ms
INFO: Project key: hello-world
INFO: Base dir: /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99
INFO: Working dir: /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99/.scannerwork
INFO: Load project settings for component key: 'hello-world'
INFO: Load project settings for component key: 'hello-world' (done) | time=32ms
INFO: Auto-configuring with CI 'Jenkins'
INFO: Load quality profiles
INFO: Load quality profiles (done) | time=60ms
INFO: Load active rules
INFO: Load active rules (done) | time=1633ms
INFO: Load analysis cache
INFO: Load analysis cache (766 bytes) | time=16ms
INFO: Load project repositories
INFO: Load project repositories (done) | time=17ms
INFO: Indexing files...
INFO: Project configuration:
INFO: 1 file indexed
INFO: 0 files ignored because of scm ignore settings
INFO: Quality profile for js: Sonar way
INFO: ------------- Run sensors on module hello-world
INFO: Load metrics repository
INFO: Load metrics repository (done) | time=32ms
INFO: Sensor JaCoCo XML Report Importer [jacoco]
INFO: 'sonar.coverage.jacoco.xmlReportPaths' is not defined. Using default locations: target/site/jacoco/jacoco.xml,target/site/jacoco-it/jacoco.xml,build/reports/jacoco/test/jacocoTestReport.xml
INFO: No report imported, no coverage information will be imported by JaCoCo XML Report Importer
INFO: Sensor JaCoCo XML Report Importer [jacoco] (done) | time=2ms
INFO: Sensor JavaScript analysis [javascript]
INFO: 1 source file to be analyzed
INFO: 1/1 source file has been analyzed
INFO: Hit the cache for 0 out of 1
INFO: Miss the cache for 1 out of 1: ANALYSIS_MODE_INELIGIBLE [1/1]
INFO: Sensor JavaScript analysis [javascript] (done) | time=9553ms
INFO: Sensor TypeScript analysis [javascript]
INFO: No input files found for analysis
INFO: Hit the cache for 0 out of 0
INFO: Miss the cache for 0 out of 0
INFO: Sensor TypeScript analysis [javascript] (done) | time=1ms
INFO: Sensor CSS Rules [javascript]
INFO: No CSS, PHP, HTML or VueJS files are found in the project. CSS analysis is skipped.
INFO: Sensor CSS Rules [javascript] (done) | time=0ms
INFO: Sensor JavaScript/TypeScript Coverage [javascript]
INFO: Analysing [/var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99/./coverage/lcov.info]
INFO: Sensor JavaScript/TypeScript Coverage [javascript] (done) | time=13ms
INFO: Sensor C# Project Type Information [csharp]
INFO: Sensor C# Project Type Information [csharp] (done) | time=1ms
INFO: Sensor C# Analysis Log [csharp]
INFO: Sensor C# Analysis Log [csharp] (done) | time=34ms
INFO: Sensor C# Properties [csharp]
INFO: Sensor C# Properties [csharp] (done) | time=0ms
INFO: Sensor HTML [web]
INFO: Sensor HTML [web] (done) | time=3ms
INFO: Sensor TextAndSecretsSensor [text]
INFO: 1 source file to be analyzed
INFO: 1/1 source file has been analyzed
INFO: Sensor TextAndSecretsSensor [text] (done) | time=13ms
INFO: Sensor VB.NET Project Type Information [vbnet]
INFO: Sensor VB.NET Project Type Information [vbnet] (done) | time=0ms
INFO: Sensor VB.NET Analysis Log [vbnet]
INFO: Sensor VB.NET Analysis Log [vbnet] (done) | time=11ms
INFO: Sensor VB.NET Properties [vbnet]
INFO: Sensor VB.NET Properties [vbnet] (done) | time=1ms
INFO: Sensor IaC Docker Sensor [iac]
INFO: 0 source files to be analyzed
INFO: 0/0 source files have been analyzed
INFO: Sensor IaC Docker Sensor [iac] (done) | time=78ms
INFO: ------------- Run sensors on project
INFO: Sensor Analysis Warnings import [csharp]
INFO: Sensor Analysis Warnings import [csharp] (done) | time=0ms
INFO: Sensor Zero Coverage Sensor
INFO: Sensor Zero Coverage Sensor (done) | time=1ms
INFO: CPD Executor Calculating CPD for 1 file
INFO: CPD Executor CPD calculation finished (done) | time=6ms
INFO: Analysis report generated in 139ms, dir size=124.0 kB
INFO: Analysis report compressed in 41ms, zip size=17.3 kB
INFO: Analysis report uploaded in 53ms
INFO: ANALYSIS SUCCESSFUL, you can find the results at: http://10.1.89.220:9000/dashboard?id=hello-world
INFO: Note that you will be able to access the updated dashboard once the server has processed the submitted analysis report
INFO: More about the report processing at http://10.1.89.220:9000/api/ce/task?id=AYsZFQc1eO8sQy3H3RTb
INFO: Analysis total time: 20.632 s
INFO: ------------------------------------------------------------------------
INFO: EXECUTION SUCCESS
INFO: ------------------------------------------------------------------------
INFO: Total time: 22.108s
INFO: Final Memory: 17M/64M
INFO: ------------------------------------------------------------------------
[Pipeline] echo
PENDING
[Pipeline] sleep
Sleeping for 3 sec
[Pipeline] sh
+ cat .scannerwork/report-task.txt
+ grep ceTaskUrl
+ sed s/ceTaskUrl=//
[Pipeline] sh
+ curl -sk -u sqa_7186f0531c72bb25612959e302c7e7ad05b54d33 'http://10.1.89.220:9000/api/qualitygates/project_status?projectKey=hello-world'
+ jq .projectStatus.status
+ sed 's/"//g'
Enter host password for user 'sqa_7186f0531c72bb25612959e302c7e7ad05b54d33':
[Pipeline] echo
waiting for sonar results
[Pipeline] sleep
Sleeping for 5 sec
[Pipeline] echo
OK
[Pipeline] sleep
Sleeping for 10 sec
[Pipeline] echo
OK
[Pipeline] }
[Pipeline] // dir
[Pipeline] }
[Pipeline] // withSonarQubeEnv
[Pipeline] }
[Pipeline] // nodejs
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (build images)
[Pipeline] script
[Pipeline] {
[Pipeline] dir
Running in /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99
[Pipeline] {
[Pipeline] echo
build images
[Pipeline] echo
10.1.89.236:50003/jpangalinan/hello-world:1.0.10
[Pipeline] sh
+ docker build -t 10.1.89.236:50003/jpangalinan/hello-world:1.0.10 .
Sending build context to Docker daemon  27.69MB

Step 1/6 : FROM nexus.corp.bankbtpn.co.id:50003/kopassus-ops/node:18-buster-slim
 ---> 4be15737f633
Step 2/6 : WORKDIR /app
 ---> Using cache
 ---> c31f18bf565c
Step 3/6 : ADD . /app
 ---> e020d3bf8f81
Step 4/6 : RUN npm i 
 ---> Running in bcdb490e33b5

up to date, audited 291 packages in 16s

31 packages are looking for funding
  run `npm fund` for details

133 low severity vulnerabilities

To address issues that do not require attention, run:
  npm audit fix

To address all issues (including breaking changes), run:
  npm audit fix --force

Run `npm audit` for details.
[91mnpm notice 
npm notice New major version of npm available! 9.5.1 -> 10.2.0
npm notice Changelog: <https://github.com/npm/cli/releases/tag/v10.2.0>
npm notice Run `npm install -g npm@10.2.0` to update!
npm notice 
[0mRemoving intermediate container bcdb490e33b5
 ---> edc797b78751
Step 5/6 : EXPOSE 3000
 ---> Running in 0c2acefe2dee
Removing intermediate container 0c2acefe2dee
 ---> a62496089f59
Step 6/6 : CMD npm start
 ---> Running in c6564d6e3a5b
Removing intermediate container c6564d6e3a5b
 ---> 667d0c0571df
Successfully built 667d0c0571df
Successfully tagged 10.1.89.236:50003/jpangalinan/hello-world:1.0.10
[Pipeline] }
[Pipeline] // dir
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Push Image to Registry)
[Pipeline] script
[Pipeline] {
[Pipeline] dir
Running in /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99
[Pipeline] {
[Pipeline] echo
push image to registry
[Pipeline] withCredentials
Masking supported pattern matches of $user or $password
[Pipeline] {
[Pipeline] sh
Warning: A secret was passed to "sh" using Groovy String interpolation, which is insecure.
		 Affected argument(s) used the following variable(s): [password, user]
		 See https://jenkins.io/redirect/groovy-string-interpolation for details.
+ docker login -u **** -p **** 10.1.89.236:50003
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /var/lib/jenkins/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
[Pipeline] sh
+ docker push 10.1.89.236:50003/jpangalinan/hello-world:1.0.10
The push refers to repository [10.1.89.236:50003/jpangalinan/hello-world]
d22b84d52b0e: Preparing
1577139ed3a1: Preparing
1e15fd38c457: Preparing
008ed4e8ae35: Preparing
27a8b3bf5873: Preparing
ff1816269110: Preparing
60fc1de3d098: Preparing
d85b356ec3b5: Preparing
27a8b3bf5873: Waiting
ff1816269110: Waiting
60fc1de3d098: Waiting
d85b356ec3b5: Waiting
1e15fd38c457: Layer already exists
008ed4e8ae35: Layer already exists
27a8b3bf5873: Layer already exists
ff1816269110: Layer already exists
d85b356ec3b5: Layer already exists
60fc1de3d098: Layer already exists
1577139ed3a1: Pushed
d22b84d52b0e: Pushed
1.0.10: digest: sha256:d9a2704c803d73513914b337a9c2373e62e91706d1b9a7ac5db6813d999810b4 size: 1996
[Pipeline] sh
+ docker logout
Removing login credentials for https://index.docker.io/v1/
[Pipeline] }
[Pipeline] // withCredentials
[Pipeline] }
[Pipeline] // dir
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (deploy)
[Pipeline] script
[Pipeline] {
[Pipeline] dir
Running in /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99
[Pipeline] {
[Pipeline] echo
deploy
[Pipeline] }
[Pipeline] // dir
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (remove old image)
[Pipeline] script
[Pipeline] {
[Pipeline] dir
Running in /var/lib/jenkins/workspace/jpangalinan/ci-cd-on-vm/99
[Pipeline] {
[Pipeline] echo
remove old image
[Pipeline] sh
+ docker rmi 10.1.89.236:50003/jpangalinan/hello-world:1.0.10
Untagged: 10.1.89.236:50003/jpangalinan/hello-world:1.0.10
Untagged: 10.1.89.236:50003/jpangalinan/hello-world@sha256:d9a2704c803d73513914b337a9c2373e62e91706d1b9a7ac5db6813d999810b4
Deleted: sha256:667d0c0571dfe0aaf7640a20953e027002eb02a328f1d3bff6631e3596966f0f
Deleted: sha256:a62496089f5939a8cdb064519b9b7afa8830a890a5af0bc882836435c0c54c9b
Deleted: sha256:edc797b7875169bff571eb6277d83fa1b20dc685cef569287f14318adce16499
Deleted: sha256:f87eb6c70be44c355ec52d03e0dc726dcd11e1bd06b222840a7ad5bb343d7878
Deleted: sha256:e020d3bf8f814af2fe202cae6014ae131a97fd2d165b714d0344709bc760952e
Deleted: sha256:f8239782ae80a19f1f0f67e363136f2710edab599e72b25fa2850ef24d79b53e
[Pipeline] }
[Pipeline] // dir
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
```