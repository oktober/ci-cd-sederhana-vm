def REGISTRY = 'url'
def PROJECT = 'project-name'
pipeline {
	agent any

	stages {

		stage('pull repo') {
			steps {
				dir("${env.BUILD_NUMBER}") { 
					checkout scm: [$class: 'GitSCM', userRemoteConfigs: [
						[credentialsId: 'GITLAB_POC', url: env.gitlabSourceRepoHttpUrl]
					], branches: [
						[name: env.gitlabSourceBranch]
					]]
					
					// Menentukan name & versi dari package.json
					script {
						def packageJson = readJSON file: 'package.json'
						env.VERSION = packageJson.version
						env.SERVICE_NAME = packageJson.name

						currentBuild.displayName = "#${BUILD_NUMBER}, ${VERSION}"
					}
					
				}
			}
		}

		stage('Build Artifact') {
			steps {
				nodejs('node') {
					dir("${env.BUILD_NUMBER}") {  
						echo 'Build Artifact'
						sh 'npm install'
						// sh 'npm run build'
						sh 'npm run test'
					}
				}
			}
		}

		stage('Code Quality Check via SonarQube') {
			steps {
				script {
					nodejs('node') {
						def scannerHome = tool 'sonarqube-dev';
						def clrsonarstatus = "PENDING";
						withSonarQubeEnv("sonarqube-dev") {
							dir("${env.BUILD_NUMBER}") { 
								sh """${tool("sonarqube-dev")}/bin/sonar-scanner \
									-Dsonar.projectKey=${SERVICE_NAME} \
									-Dsonar.sources=src \
									-Dsonar.javascript.lcov.reportPaths=./coverage/lcov.info \
									-Dsonar.host.url=url \
									-Dsonar.login=token """
								
								echo "${clrsonarstatus}"
								sleep(time:3,unit:"SECONDS")

								while ( clrsonarstatus == "PENDING" || clrsonarstatus == "IN_PROGRESS" ) {
									def sonarstatusurl = sh(script: "cat .scannerwork/report-task.txt | grep ceTaskUrl | sed 's/ceTaskUrl=//'", returnStdout: true)
									def clrsonarstatusurl = sonarstatusurl.replaceAll("\\r\\n|\\r|\\n", "")
									def sonarstatus = sh(script: "curl -sk -u <token> 'http://10.1.89.220:9000/api/qualitygates/project_status?projectKey=${SERVICE_NAME}' | jq \'.projectStatus.status\' | sed  \'s/\"//g\'", returnStdout: true)
									clrsonarstatus = sonarstatus.replaceAll("\\r\\n|\\r|\\n", "")
									println "waiting for sonar results"
									sleep(5)
								}

                                // check coverage status
								if (clrsonarstatus == "ERROR") {
									echo "${clrsonarstatus}"
									error("The Error message: Please check your coverage")
								} else {
									echo "${clrsonarstatus}"
								}

								
							}
						}
					}
				}
			}
		}

		stage('Build Image') {
			steps {
				script {
					dir("${env.BUILD_NUMBER}") {
						echo "${REGISTRY}/${PROJECT}/${SERVICE_NAME}:${VERSION}"
						sh "docker build -t ${REGISTRY}/${PROJECT}/${SERVICE_NAME}:${VERSION} ."

					}
				}
			}
		}

		stage('Push Image to Registry') {
			steps {
				script {
					dir("${env.BUILD_NUMBER}") {
                        	echo 'push image to registry'
                            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'CREDENTIALS', usernameVariable: 'user', passwordVariable: 'password']]) {
                                sh "docker login -u $user -p $password ${REGISTRY}"
                                sh "docker push ${REGISTRY}/${PROJECT}/${SERVICE_NAME}:${VERSION}"
                                sh "docker logout"
                            }
					}
				}
			}
		}

		stage('Deploy') {
			steps {
				script {
					dir("${env.BUILD_NUMBER}") {
						// deploy process here
						echo 'deploy'
					}
				}
			}
		}

		stage('Remove old image') {
			steps {
				script {
					dir("${env.BUILD_NUMBER}") {
						echo 'remove old image'
                        sh "docker rmi ${REGISTRY}/${PROJECT}/${SERVICE_NAME}:${VERSION}"
					}
				}
			}
		}
	}
}
